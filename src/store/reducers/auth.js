import * as actionTypes from "../actions/actionTypes";
import {updateObject} from "../../shared/utility";

const initialState = {
    token: null,
    expiredAt: null
};

const storeToken = (state, action) => {
    return updateObject(state, {
        token: action.token,
        expiredAt: action.expiredAt
    });
};

const auth = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_TOKEN:
            return storeToken(state, action);
        default:
            return state;
    }
};

export default auth;