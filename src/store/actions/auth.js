import * as actionTypes from "./actionTypes";

export const storeToken = (token, expiredAt) => {
    return {
        type: actionTypes.STORE_TOKEN,
        token, expiredAt
    };
};