import React, {Component} from "react";
import {SafeAreaView, StyleSheet, Text, TextInput, View} from "react-native";
import Button from "./Button";
import {Colors} from "react-native/Libraries/NewAppScreen";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {sendHttpRequest} from "../shared/utility";
import {connect} from "react-redux";
import * as actions from "../store/actions";

class LoginPage extends Component {

    componentDidUpdate(prevProps) {
    }

    onLogin = () => {
        const httpBody = JSON.stringify({
            email: "nghiavt.topcv@gmail.com",
            password: "123456"
        });
        sendHttpRequest(
            "/api/login",
            "POST",
            {},
            httpBody
        )
            .then(res => {
                if (res[0] === 200) {
                    this.props.storeToken(res[1].token, res[1].expiredAt);
                    this.props.navigation.navigate("HomeStack");
                }
            })
            .catch(err => console.log("Caught error:", err));

    };

    render() {
        return (
            <SafeAreaView style={{
                flex: 1,
                margin: 25,
                padding: 16,
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "stretch"
            }}>
                <Icon
                    name="clipboard-text-outline"
                    size={100}
                    style={{
                        transform: [{rotateZ: "16deg"}],
                        alignSelf: "center"
                    }}
                />
                <Text style={styles.title}>Log in</Text>

                {/* Login Form */}
                <View style={{
                    flexDirection: "column",
                    height: "40%",
                    padding: 30,
                    margin: 20,
                    backgroundColor: "white",
                    shadowColor: "black",
                    shadowOpacity: 0.25,
                    shadowOffset: {width: 0, height: 1},
                    shadowRadius: 4
                }}>
                    <Text style={styles.sectionTitle}>Email</Text>
                    <TextInput style={styles.textInput} placeholder="Email dang nhap"/>
                    <Text style={styles.sectionTitle}>Password</Text>
                    <TextInput style={styles.textInput} placeholder="Mat khau"/>
                    <Button
                        onPress={this.onLogin}
                        title="Log in"
                        style={{backgroundColor: "#41DAB5", ...styles.button}}/>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter
    },
    engine: {
        position: "absolute",
        right: 0
    },
    body: {
        backgroundColor: Colors.white
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24
    },
    title: {
        // fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "900",
        fontSize: 24,
        lineHeight: 28,
        display: "flex",
        alignItems: "center",
        textAlign: "center",
        color: "#333"
    },
    sectionTitle: {
        fontStyle: "normal",
        fontWeight: "900",
        fontSize: 16,
        lineHeight: 19,
        display: "flex",
        alignItems: "center",
        marginTop: 16,
        letterSpacing: -0.333333
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: "400",
        color: Colors.dark
    },
    highlight: {
        fontWeight: "700"
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: "600",
        padding: 4,
        paddingRight: 12,
        textAlign: "right"
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: "#737373",
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    button: {
        marginVertical: 20,
        marginHorizontal: 20,
        marginBottom: 8,
        paddingVertical: 8,
        borderRadius: 4
    },
    textInput: {
        backgroundColor: "white",
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#CACACA",
        height: 40,
        margin: 10,
        padding: 8
    }
});

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        expiredAt: state.auth.expiredAt
    };
};

const mapDispatchToProps = dispatch => {
    return {
        storeToken: (token, expiredAt) => dispatch(actions.storeToken(token, expiredAt))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);