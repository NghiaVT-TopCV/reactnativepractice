/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from "react";
import {Alert, FlatList, SafeAreaView, StyleSheet, Text, View} from "react-native";
import Button from "./Button";
import {Colors} from "react-native/Libraries/NewAppScreen";
import {connect} from "react-redux";
import Todo from "./Todo";
import EditTodo from "./EditTodo";
import * as actions from "../store/actions";
import {addTokenPrefix, isSuccessful, sendHttpRequest} from "../shared/utility";

class TodoList extends Component {
    state = {
        nDone: 0,
        nPending: 0,
        Authorization: null
    };

    componentDidMount() {

        this.props.fetchTodos();
    }

    componentDidUpdate(prevProps) {
        if (this.props.todos !== prevProps.todos) {
            this.setTodoStats();
        }
    }

    fetchTodos = () => {
        if (!this.props.token) {
            console.log("Not logged in.");
            this.props.navigation.navigate("AuthStack");
        } else {
            const Authorization = addTokenPrefix(this.props.token);
            this.setState({Authorization});

            sendHttpRequest(
                "/api/todo/list",
                "GET",
                {})
                .then(res => {
                    if (isSuccessful(res)) {
                        const todos = res[1].todos;
                        this.props.storeTodos(todos);
                    }
                })
                .catch(error => console.log("Error:", error));
        }
        this.setTodoStats();
    };

    setTodoStats = () => {
        const nDone = this.props.todos.filter(item => item.isDone).length;
        const nPending = this.props.todos.length - nDone;
        this.setState({nDone, nPending});
    };

    handleToggleTodo = index => {
        this.props.onToggleTodo(index);

    };

    handleDeleteTodo = index => {
        Alert.alert(
            "Alert",
            "Are you sure you want to delete that TODO?",
            [
                {text: "Cancel", style: "cancel"},
                {
                    text: "Delete",
                    onPress: () => this.props.onDeleteTodo(index)
                }
            ],
            {cancelable: true}
        );
    };

    handleSubmitTodo = (index, content) => {
        this.props.onSubmitTodo(index, content);
    };

    render() {
        return (
            <SafeAreaView style={{flex: 1, margin: 25, padding: 16}}>
                <EditTodo
                    ref={r => this.EditTodo = r}
                    onSubmit={this.handleSubmitTodo}/>
                <View style={{flexDirection: "row"}}>
                    <Text style={{...styles.title, flex: 5}}>Danh sách ghi chú</Text>
                    <Button
                        style={{flex: 1}}
                        icon="pencil-box-outline"
                        size={30}
                        onPress={() => this.handleEditTodoStart(-1)}>
                    </Button>
                </View>

                <View style={{flex: 1}}>
                    <Text style={styles.sectionTitle}>Thong ke</Text>
                    <Text>Da hoan thanh: {this.state.nDone}</Text>
                    <Text>Chua hoan thanh: {this.state.nPending}</Text>
                </View>
                <View style={{flex: 9}}>
                    <Text style={styles.sectionTitle}>Danh sach</Text>
                    <FlatList
                        data={this.props.todos}
                        keyExtractor={item => item._id}
                        renderItem={({item, index}) => {
                            return (
                                <Todo
                                    item={item}
                                    key={item._id}
                                    onToggle={() => this.handleToggleTodo(index)}
                                    onDelete={() => this.handleDeleteTodo(index)}
                                    onEditStart={() => this.handleEditTodoStart(index)}
                                />);
                        }}
                    />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter
    },
    engine: {
        position: "absolute",
        right: 0
    },
    body: {
        backgroundColor: Colors.white
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24
    },
    title: {
        // fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "900",
        fontSize: 20,
        lineHeight: 23,
        display: "flex",
        alignItems: "center",
        textAlign: "left",
        color: "#333"
    },
    sectionTitle: {
        fontStyle: "normal",
        fontWeight: "900",
        fontSize: 16,
        lineHeight: 19,
        display: "flex",
        alignItems: "center",
        letterSpacing: -0.333333
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: "400",
        color: Colors.dark
    },
    highlight: {
        fontWeight: "700"
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: "600",
        padding: 4,
        paddingRight: 12,
        textAlign: "right"
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: "#737373",
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    itemList: {
        flexDirection: "row",
        alignItems: "stretch",
        justifyContent: "space-between",
        marginTop: 8,
        marginHorizontal: 20
    },
    button: {
        flex: 1,
        marginHorizontal: 20,
        marginBottom: 8,
        paddingVertical: 8,
        borderRadius: 4
    },
    todoBox: {
        backgroundColor: "white",
        shadowOffset: {width: 0, height: 1},
        shadowRadius: 4,
        shadowColor: "black",
        shadowOpacity: 0.25,
        borderRadius: 8,
        margin: 8
    }
});

const mapStateToProps = state => {
    return {
        todos: state.todoList.todos,
        token: state.auth.token,
        expiredAt: state.auth.expiredAt
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmitTodo: (index, content) => dispatch(actions.submitTodo(index, content)),
        onToggleTodo: index => dispatch(actions.toggleTodo(index)),
        onDeleteTodo: index => dispatch(actions.deleteTodo(index)),
        storeTodos: todos => dispatch(actions.storeTodos(todos)),
        fetchTodos: () => dispatch(actions.fetchTodos())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
