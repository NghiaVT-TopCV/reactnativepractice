import {combineReducers} from "redux";
import todoList from "./todoList";
import auth from "./auth";

export default combineReducers({
    todoList, auth
});