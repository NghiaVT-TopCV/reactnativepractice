/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import TodoList from "./src/components/TodoList";
import LoginPage from "./src/components/LoginPage";
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";
import "react-native-gesture-handler";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from "redux-saga";
import {rootSaga} from "./src/saga";
import "regenerator-runtime/runtime";
import rootReducer from "./src/store/reducers";
import {setStore} from "./src/shared/utility";

const AuthStack = createStackNavigator(
    {
        LoginPage: LoginPage
    },
    {
        headerMode: "none"
    }
);

const HomeStack = createStackNavigator(
    {
        TodoList: TodoList
    },
    {
        headerMode: "none"
    }
);

const RootSwitch = createSwitchNavigator(
    {
        AuthStack,
        HomeStack
    },
    {
        initialRouteName: "AuthStack"
    }
);

const Navigation = createAppContainer(RootSwitch);
const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
setStore(store);

sagaMiddleware.run(rootSaga);

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Navigation/>
            </Provider>
        );
    }
}

