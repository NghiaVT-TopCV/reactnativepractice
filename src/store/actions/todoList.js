import * as actionTypes from "./actionTypes";

let nextTodoId = 2;

export const submitTodo = (index, content) => {
    if (index === -1) {
        return createTodo(content);
    } else {

        return updateTodo(index, content);
    }
};

export const deleteTodo = index => {
    return {
        type: actionTypes.DELETE_TODO,
        index
    };
};

export const toggleTodo = index => {
    return {
        type: actionTypes.TOGGLE_TODO,
        index
    };
};

export const storeTodos = todos => {
    return {
        type: actionTypes.STORE_TODOS,
        todos
    };
};

function createTodo(content) {
    return {
        type: actionTypes.CREATE_TODO,
        content
    };
}


function updateTodo(index, content) {
    return {
        type: actionTypes.UPDATE_TODO,
        index,
        content
    };
}


export const fetchTodos = () => {
    return {
        type: actionTypes.FETCH_TODOS
    };
};