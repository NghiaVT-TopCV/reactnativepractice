import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";


const Button = props => {
    return <TouchableOpacity
        {...props}
        style={{ ...props.style, alignContent: "center" }}>
        {props.icon ? <Icon name={props.icon} size={props.size} /> : null}
        <Text style={{ alignSelf: "center", color: "white" }}>{props.title}</Text>
    </TouchableOpacity>
};

export default Button;