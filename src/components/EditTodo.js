import React, {Component} from "react";
import {Alert, Modal, StyleSheet, TextInput, View} from "react-native";
import Button from "./Button";

export default class TodoEdit extends Component {
    state = {
        isModalVisible: false,
        todoIndex: -1,
        todocontent: "Do something..."
    };

    setModalVisible(isVisible) {
        this.setState({isModalVisible: isVisible});
    }

    handleChangeText = text => {
        this.setState({todocontent: text});
    };

    focusTodoIndex = todoIndex => {
        this.setState({todoIndex});
    };

    render() {
        const {todocontent} = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.isModalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                }}>
                <View style={{
                    flex: 1,
                    backgroundColor: "rgba(0,0,0,0.3)",
                    justifyContent: "center",
                    alignItems: "center",
                }}>
                    <View
                        style={{
                            padding: 18,
                            width: "90%",
                            height: 200,
                            backgroundColor: "white",
                            flexDirection: "column",
                        }}>
                        <TextInput
                            style={{
                                flex: 3,
                                padding: 8,
                                alignItems: "flex-start",
                                backgroundColor: "#FFFFFF",
                                borderColor: "#CACACA",
                                borderStyle: "solid",
                                borderWidth: 1,
                                borderRadius: 8,
                            }}
                            onChangeText={this.handleChangeText}
                            value={todocontent}
                        />
                        <View style={{
                            flex: 2,
                            flexDirection: "row",
                        }}>
                            <Button
                                style={{backgroundColor: "#626262", ...styles.button}}
                                title="Cancel"
                                onPress={() => this.setModalVisible(false)}/>
                            <Button
                                style={{backgroundColor: "#41DAB5", ...styles.button}}
                                title="Ok"
                                onPress={() => {
                                    this.props.onSubmit(
                                        this.state.todoIndex,
                                        this.state.todocontent);
                                    this.setModalVisible(false);
                                }}/>
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        flex: 1,
        // marginHorizontal: 20,
        margin: 8,
        justifyContent: "center",
        // paddingVertical: 8,
        borderRadius: 4,
    },
});