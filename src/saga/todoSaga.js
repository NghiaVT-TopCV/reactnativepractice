import * as actionTypes from "../store/actions/actionTypes";
import {isSuccessful, sendHttpRequest, store} from "../shared/utility";
import {put, takeEvery} from "redux-saga/effects";

export function* watchFetchTodos() {
    yield takeEvery(actionTypes.FETCH_TODOS, fetchTodos);
}

export function* watchDeleteTodo() {
    yield takeEvery(actionTypes.DELETE_TODO, deleteTodo);
}

export function* watchCreateTodo() {
    yield  takeEvery(actionTypes.CREATE_TODO, createTodo);
}

export function* watchUpdateTodo() {
    yield takeEvery(actionTypes.UPDATE_TODO, updateTodo);
}

export function* watchToggleTodo() {
    yield takeEvery(actionTypes.TOGGLE_TODO, toggleTodo);
}

function* fetchTodos() {
    try {
        let response = yield sendHttpRequest(
            "/api/todo/list",
            "GET");
        console.log("Response:", response);
        if (response[0] === 200) {
            yield put({
                type: actionTypes.STORE_TODOS,
                todos: response[1].todos
            });
        }

    } catch (e) {
        console.log("Error fetching:", e);
    }
}

function* deleteTodo(action) {
    try {
        const _id = store.getState().todoList.todos[action.index]._id;
        console.log("Deleting:", _id);
        let result = yield sendHttpRequest(
            "/api/todo/" + _id,
            "DELETE"
        );
        if (isSuccessful(result)) {
            yield put({type: actionTypes.FETCH_TODOS});
        }
    } catch (e) {
        console.log("Error deleting: ", e);
    }
}

function* createTodo(action) {
    try {
        const body = JSON.stringify({content: action.content});
        let result = yield sendHttpRequest(
            "/api/todo/add/",
            "POST",
            {},
            body);
        if (isSuccessful(result)) {
            yield put({type: actionTypes.FETCH_TODOS});
        }
    } catch (e) {
        console.log("Error creating todo:", e);
    }
}

function* updateTodo(action) {
    try {
        const _id = store.getState().todoList.todos[action.index]._id;
        const body = JSON.stringify({content: action.content});
        const result = yield sendHttpRequest(
            "/api/todo/" + _id,
            "PATCH",
            {},
            body);
        console.log("Response:", result);
        if (isSuccessful(result)) {
            yield put({type: actionTypes.FETCH_TODOS});
        }
    } catch (e) {
        console.log("Error updating:", e);
    }
}

function* toggleTodo(action) {
    try {
        const todo = store.getState().todoList.todos[action.index];
        const _id = todo._id;
        const body = JSON.stringify({
            content: todo.content,
            status: todo.isDone ? 0 : 1
        });
        const result = yield sendHttpRequest(
            "/api/todo/" + _id,
            "PATCH",
            {},
            body);
        console.log("Response:", result);
        if (isSuccessful(result)) {
            yield put({type: actionTypes.FETCH_TODOS});
        }
        console.log("Toggling todo:", todo);
    } catch (e) {
        console.log("Error toggling: ", e);
    }
}

