import {all, fork} from "redux-saga/effects";
import {watchCreateTodo, watchDeleteTodo, watchFetchTodos, watchToggleTodo, watchUpdateTodo} from "./todoSaga";

export function* rootSaga() {
    yield all([
        fork(watchFetchTodos),
        fork(watchDeleteTodo),
        fork(watchCreateTodo),
        fork(watchUpdateTodo),
        fork(watchToggleTodo)
    ]);
}