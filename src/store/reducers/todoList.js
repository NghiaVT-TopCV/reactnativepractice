import * as actionTypes from "../actions/actionTypes";
import {updateObject} from "../../shared/utility";

const initialState = {
    todos: []
};

const fetchTodosStart = (state, action) => {
    return state;
};

// const createTodo = (state, action) => {
//     const todos = [...state.todos];
//     todos.push({
//         id: action._id.toString(),
//         content: action.content,
//         isDone: false
//     });
//
//     return updateObject(state, {todos});
// };

const updateTodo = (state, action) => {
    const todos = [...state.todos];
    todos[action.index].content = action.content;
    return updateObject(state, {todos});
};

const deleteTodo = (state, action) => {
    const todos = [...state.todos];
    todos.splice(action.index, 1);
    return updateObject(state, {todos});
};

const storeTodos = (state, action) => {
    return updateObject(state, {todos: action.todos});
};

const todoList = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_TODOS:
            return fetchTodosStart(state, action);
        case actionTypes.STORE_TODOS:
            return storeTodos(state, action);
        // case actionTypes.CREATE_TODO:
        //     return createTodo(state, action);
        // case actionTypes.UPDATE_TODO:
        //     return updateTodo(state, action);
        default:
            return state;
    }
};

export default todoList;