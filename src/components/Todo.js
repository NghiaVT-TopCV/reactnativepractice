import React from "react";
import {StyleSheet, Text, View} from "react-native";
import Button from "./Button";


const Todo = ({item, onToggle, onDelete, onEditStart}) => {
	return (
		<View style={styles.todoBox}>
			<View style={{...styles.itemList}}>
				<Text style={{flex: 10}}>{item.content}</Text>
				<Button
					style={{flex: 1}}
					onPress={onToggle}
					icon={item.isDone
						? "check-box-outline"
						: "checkbox-blank-outline"}
					size={25}
				/>
            </View>
            <View style={{ flexDirection: "row", marginVertical: 8 }}>
                <Button title="Delete" onPress={onDelete}
                    style={{ backgroundColor: "#EC5D5D", ...styles.button }} />
                <Button title="Edit" onPress={onEditStart}
                    style={{ backgroundColor: "#41DAB5", ...styles.button }} />
            </View>
        </View>);
};

const styles = StyleSheet.create({
    itemList: {
		flexDirection: 'row',
		alignItems: 'stretch',
		justifyContent: "space-between",
		marginTop: 8,
		marginHorizontal: 20,
	},
	button: {
		flex: 1,
		marginHorizontal: 20,
		marginBottom: 8,
		paddingVertical: 8,
		borderRadius: 4
	},
	todoBox: {
		backgroundColor: "white",
		shadowOffset: { width: 0, height: 1 },
		shadowRadius: 4,
		shadowColor: "black",
		shadowOpacity: 0.25,
		borderRadius: 8,
		margin: 8,
	}

});

export default Todo;
